// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

contract AssinpaFT is ERC20, ERC20Burnable {
    constructor() ERC20("AssinpaFT", "ECBN") {
        uint256 n = 100000;
        _mint(msg.sender, n * 10**uint256(decimals()));
    }
}
