require('dotenv').config();

const hre = require("hardhat");
async function main() {
  const NFT = await hre.ethers.getContractFactory("AssinpaFT");  
  
  const WALLET_ADDRESS = "0x3093560Bdc28D293eac96112C98f3eF6cce47a5F"
  const WALLET_RECEIVER_ADDRESS = "0x8e20659c2C149B7C0aa5230Dc276653A4de10162"
  const CONTRACT_ADDRESS = process.env.CONTRACT_ADDRESS
  const contract = NFT.attach(CONTRACT_ADDRESS);
  let result = await contract.allowance(WALLET_ADDRESS, WALLET_RECEIVER_ADDRESS)
  // result = await contract.approve(WALLET_ADDRESS, 500)
  // console.log("FT approve:", result);
  console.log("FT alowance:", result);
}
main().then(() => process.exit(0)).catch(error => {
  console.error(error);
  process.exit(1);
});