require('dotenv').config();
const hre = require("hardhat");

async function main() {
  const FT = await hre.ethers.getContractFactory("AssinpaFT");

  //The IPFS Address of your image that you uploaded to Pinata

  const QUANT = 10000
  //Your Wallet Address that contains Matic whose private key you put into .gitignore
  const WALLET_ADDRESS = "0x3093560Bdc28D293eac96112C98f3eF6cce47a5F"
  // const WALLET_MATEUS_ADDRESS = "0x8e20659c2C149B7C0aa5230Dc276653A4de10162"
  //The AssinpaFT contract address that you deployed above
  const CONTRACT_ADDRESS = process.env.CONTRACT_ADDRESS

  const contract = FT.attach(CONTRACT_ADDRESS);
  await contract.transfer(WALLET_ADDRESS, QUANT);
  console.log("FT minted:", contract);
}

main().then(() => process.exit(0)).catch(error => {
  console.error(error);
  process.exit(1);
});