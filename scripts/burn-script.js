require('dotenv').config();
const hre = require("hardhat");

async function main() {
  const FT = await hre.ethers.getContractFactory("AssinpaFT");
  const QUANT = 1
  const WALLET_ADDRESS = "0x3093560Bdc28D293eac96112C98f3eF6cce47a5F"
  const CONTRACT_ADDRESS = process.env.CONTRACT_ADDRESS

  const contract = FT.attach(CONTRACT_ADDRESS);
  let burnResult = await contract.burnFrom(WALLET_ADDRESS, QUANT);
  console.log("FT burned:", burnResult);
}

main().then(() => process.exit(0)).catch(error => {
  console.error(error);
  process.exit(1);
});