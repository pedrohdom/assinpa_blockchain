const hre = require("hardhat");

async function main() {
  const FT = await hre.ethers.getContractFactory("AssinpaFT");
  const cont = await FT.deploy();

  await cont.deployed();

  console.log("Assinpa FT deployed to:", cont.address);
}

main().then(() => process.exit(0)).catch(error => {
  console.error(error);
  process.exit(1);
});