require('dotenv').config();
const hre = require("hardhat");

const WALLET_ADDRESS = ""
const WALLET_RECEIVER_ADDRESS = ""

async function main() {
  const NFT = await hre.ethers.getContractFactory("AssinpaFT");
  const CONTRACT_ADDRESS = process.env.CONTRACT_ADDRESS
  const QUANT = 500;
  const contract = NFT.attach(CONTRACT_ADDRESS);
  const result = await contract.transferFrom(WALLET_ADDRESS, WALLET_RECEIVER_ADDRESS, QUANT)
  console.log("FT transferred:", result);  
}
main().then(() => process.exit(0)).catch(error => {
  console.error(error);
  process.exit(1);
});