require('dotenv').config();
const hre = require("hardhat");

async function main() {
  const FT = await hre.ethers.getContractFactory("AssinpaFT");
  const WALLET_ADDRESS = "0x3093560Bdc28D293eac96112C98f3eF6cce47a5F"
  const WALLET_RECEIVER_ADDRESS = "0x8e20659c2C149B7C0aa5230Dc276653A4de10162"
  const CONTRACT_ADDRESS = process.env.CONTRACT_ADDRESS

  const contract = FT.attach(CONTRACT_ADDRESS);
  let burnResult = await contract.balanceOf(WALLET_RECEIVER_ADDRESS);
  console.log("FT balanceOf:", burnResult);
}

main().then(() => process.exit(0)).catch(error => {
  console.error(error);
  process.exit(1);
});