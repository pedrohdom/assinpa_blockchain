# Contact

email: pedroccavalcante@gmail.com / spectro@eumostro.com.br
phone: +55 41 995445332

# Steps

1) create an account on PolygonScan (polygonscan.com)

2) create an .env

```
PRIVATE_KEY=(Metamask wallet private key)
POLYGONSCAN_API_KEY=(POLYGON SCAN API KEY)
```

# Basic Sample Hardhat Project

This project demonstrates a basic Hardhat use case. It comes with a sample contract, a test for that contract, a sample script that deploys that contract, and an example of a task implementation, which simply lists the available accounts.

Try running some of the following tasks:

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
node scripts/sample-script.js
npx hardhat help
```
